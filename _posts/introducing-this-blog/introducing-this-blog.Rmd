---
title: "Introducing this blog"
description: |
  I talk about how Twitter forced me to start a blog and the two ideas I have for it.
date: '2019-05-19'
output:
  distill::distill_article:
    self_contained: false
---

On Friday, I posted this on my Twitter: 



<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Speaking of <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a>: <br>For every 5 likes of this tweet, I&#39;ll publish one of my earliest <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a> scripts (2012-2013) in a public github gist and will try to review what was going on in my mind.<br>I&#39;ll do one regardless of likes but just to see whether this is interesting to folks.</p>&mdash; Frie (@ameisen_strasse) <a href="https://twitter.com/ameisen_strasse/status/1129393207333261318?ref_src=twsrc%5Etfw">May 17, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


It kind of escalated quickly, so as of writing this, I have at least ~~four~~ five old R scripts to upload and discuss.
While this seemed a bit daunting at first, it was the final nudge I needed to finally get this blog up and running. Of course, I could've written Twitter threads but let's be honest, nobody wants to read a 20-tweet Twitter thread when they can have a nice little blog post (at least not me). Plus, this format gives me even more room to ramble around and about. So, it's a win-win. 

So without further ado, welcome to my first blog post in which I want to tell you about two things I want to do with this blog. 


## My old R scripts 

The first thing I want to do is of course to discuss some of my old R scripts. After all, I promised to do so. 

I actually have the first post of this mini series almost finished. HOWEVER, I only realized at the end of writing that probably nothing of this script was written by me but that it was instead completely provided to us by our lecturer. So, I am a bit concerned about publishing it without his permission. Not only due to actual copyright issues but also simply because it is not my code. But I'll check back with him in the coming days and hopefully get a go from him. Otherwise, I'll have to throw away some hours of work and start again with some other R script. Keep your fingers crossed.

Anyway, let me shortly talk about the background of this idea:

Last Tuesday, I had a Slack call with my friend Jan. Originally, we wanted to discuss the next steps on our several joint projects ([sealr R package](https://github.com/jandix/sealr), [CorrelAid](https://correlaid.org/) stuff) but somehow - I really don't remember how it got there - I ended up opening some really old R files. 
Looking at my earliest attempts at writing R code was incredibly cringeworthy at the beginning. But after some moments I realized that this is nothing to be ashamed, embarrassed or weirded out about. This is me, over 6 years ago, starting to write code. Of course, I had no clue. And to be honest, it took over two years and one hell of a Bachelor thesis struggle until I started to feel kind of comfortable writing R code.


So I am doing this series in order to ...

- ... show that there is no shame in being a beginner and not knowing things. We all start somewhere and our first attempts will almost always be imperfect. We will not understand stuff and that is ok. 
- ... be more compassionate towards R users who are just starting out. I hope I am already quite good at that but there is always room for being more understanding. By going back to my early days and remembering my own struggles, I hope to find more empathy for those of the community who are at the beginning of their R journeys.  
- ... be more forgiving to my younger self. Sometimes when I revisit old projects of mine, I get upset at my younger self for writing such "shitty" code and I often end up kind of angrily refactoring a lot to make it "better". Refactoring code is something I hold dear to my heart because I consider it one of the central and most interesting parts of writing code. However, I think it can and should be done without getting upset with my younger self. This series will be a good exercise in doing so.


And of course, just to finally start a blog (see above) and have a space where I can do one of my favorite activities: ramble about R. `r emo::ji("wink")` 

Ps: If you want to know more about Compassionate Coding, follow [April Wensel](https://twitter.com/aprilwensel). She's good at pointing out all the traps we tend to fall into as more experienced coders. To me, for example, it involves constantly consciously reminding myself that:

<a href="https://imgflip.com/i/31cs0r"><img src="https://i.imgflip.com/31cs0r.jpg" title="made at imgflip.com"/></a>


## Data science texts

The second thing I have in mind for this blog is answering texts. Not "texts" like in *long, boring journal papers* but as in *text messages*. I have collected some of the questions that friends have sent me over Telegram, Whatsapp, Signal or Slack, and I'll publish them (with the sender's consent of course) and present and discuss the (hopefully helpful) advice I gave the person.

I think this is is interesting because the questions I get are often less technical and more opinion oriented. Those questions are rarely discussed on popular sites like Stackoverflow because they'll be quickly discarded as "off topic" or "discussion" which is not allowed there. And while I do think that it is a valid decision to *not* allow those kind of questions on sites like Stackoverflow, they're still questions that people have and that deserve to be answered. After all, a lot of the non-technical knowledge surrounding a language is just as important as knowing how to debug that one annoying error. We'll see a good example of this in the first of those blog posts which will be on using curl to download a file protected by simple authentication.

At this place, a shoutout to my good friend [Pablo](https://twitter.com/pablocalv) is needed. He is responsible for the majority of the material and all his questions originally led to this idea back in February. Also, he told me some days ago that I am apparently known as his personal "R guru" at his university department. Lifegoal achieved, I'd say. `r emo::ji("wink")` `r emo::ji("tada")`

The "data science texts" posts will most likely differ in length and elaboration quite a bit. I have a draft on "to for loop or not to for loop" that is basically a full-blown mini essay but there are also definitely shorter ones coming up as well. The point is of those posts is definitely not to be an objective Q&A with my answer being the best answer. That is what Stackoverflow is for. Instead, the focus will be more on how I personally think about programming and solving problems in R. 

## End

This is it for today. I hope you're not too disappointed that this is actually not the first "my old R scripts" post. But as I said, I hope it'll go online in a few days once I have received an answer from my old professor. 

Until then: keep coding. `r emo::ji("heart")`

